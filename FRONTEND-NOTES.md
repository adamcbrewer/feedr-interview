## Me
Name: Adam Brewer

Email: adam@brewerlogic.com

Time: 2.5-3 hrs

## Notes

- As noted, I usually prefer to use Redux, but for something this small I was tempted to use React Context. However, creating mini reducers/actions for the context would have added extra time without showing anything significant.
- If not using internal state, the `add` and `remove` methods would ideally be reducers with proper unit tests
- I normally always use prop validation and default props
- I would normally use styled-components and keep local component styles contained, rather than a global App.css, but since simply importing plain css styles into any component is 'globally available' anyway there was no real benefit to moving some styles around.
- I used react-beautiful-dnd because the examples seemed simpler (render props) to work with quickly, although I wasted about 30 mins chasing down a ref/innerRef issue in MenuItem! Implementing DnD was mostly the reason for the overtime. I don't have any preference over react-dnd otherwise.


## Thanks

I would be happy to show a live example of a product I've recently built (almost live) which I think would further demonstrate my skills - please just let me know as it's not public yet.

Thanks for your time and please let me know if you have any questions. As mentioned I'm happy to provide further code samples (including the example mentioned), but I have further examples of my work on [brewerlogic.com](https://goo.gl/Nw59yq).

Thanks,

Adam  🙂

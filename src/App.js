import React from 'react';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';

import './App.css';

import items from './items.js';

import { DraggableMenuItem } from './MenuItem';
import DietaryAttribute from './DietaryAttribute';

const AVAILABLE_ITEMS_KEY = 'availableItems';
const SELECTED_ITEMS_KEY = 'selectedItems';

export default class App extends React.Component {

    constructor(props) {
        super(props);
        // I would not use local state usually, but rather Redux or for
        // something a little simpler (like this) React Context.
        // Using state here just allows less code to achieve the purpose
        // of adding/removing
        this.state = {
            [SELECTED_ITEMS_KEY]: [],
            [AVAILABLE_ITEMS_KEY]: items,
            store: items,
        };
    }

    onRemoveItem = id => () => this.removeItem(id)
    onAddItem = id => () => this.addItem(id)

    // I would prefer a little more finesse here if time allowed
    onDragEnd = ({ source, destination }) => {
        if (source.droppableId !== destination.droppableId) {
            if (source.droppableId === AVAILABLE_ITEMS_KEY) {
                this.addItem(this.getItemId(AVAILABLE_ITEMS_KEY, source.index), destination.index);
            } else {
                this.removeItem(this.getItemId(SELECTED_ITEMS_KEY, source.index), destination.index);
            }
        } else {
            // Should the user be able to reorder items?
        }
    }

    removeItem(id, index = 0) {
        const storeItem = this.state.store.find(item => item.id === id);
        // Naturally I'm cloning the selected items to make
        // sure we dont' mutabte the original state
        const selectedItems = [...this.selectedItems];
        const availableItems = [...this.availableItems];

        const selectedPos = selectedItems.findIndex(item => item.id === id);

        selectedItems.splice(selectedPos, 1);
        availableItems.splice(index, 0, storeItem);

        this.setState({
            selectedItems,
            availableItems,
        });
    }

    addItem(id, index = 0) {
        const storeItem = this.state.store.find(item => item.id === id);
        // Again, we don't want to mutate any state
        const selectedItems = [...this.selectedItems];
        const availableItems = [...this.availableItems];

        const availableItemIndex = availableItems.findIndex(item => item.id === id);

        selectedItems.splice(index, 0, storeItem);
        availableItems.splice(availableItemIndex, 1);

        this.setState({
            [SELECTED_ITEMS_KEY]: selectedItems,
            [AVAILABLE_ITEMS_KEY]: availableItems,
        });
    }

    // Returns the item id from the specified list
    getItemId(list = AVAILABLE_ITEMS_KEY, index = 0) {
        const item = this.state[list][index];
        return item ? item.id : null;
    }

    get availableItems() {
        return this.state[AVAILABLE_ITEMS_KEY];
    }

    get selectedItems() {
        return this.state[SELECTED_ITEMS_KEY];
    }

    get selectedTotal() {
        return this.selectedItems.length;
    }

    get selectedItemIds() {
        return this.selectedItems.map(item => item.id);
    }

    get selectedDietaryTotals() {

        // Returns a single array from all the dietary attributes of
        // all the selected items
        const allDietaries = this.selectedItems.reduce((_dietaries, item) =>
            _dietaries.concat(item.dietaries), []);

        // Creates a map of all the selected, incrementing their values
        // for each occurance found
        return allDietaries.reduce((_dietaryMap, dietary) => {
            _dietaryMap[dietary] = Number.isInteger(_dietaryMap[dietary]) // because `0` is a false
                ? _dietaryMap[dietary]
                : 0;
            _dietaryMap[dietary]++;
            return _dietaryMap;
        }, {});
    }

    renderAvailableMenuItems() {
        const { availableItems } = this;
        // I'm not happy about this level of nesting, but if I had more time I
        // would make a component for ul.item-picker, both a simple version
        // and drag-n-drop version. I feel the MenuItem example is enough as
        // an example
        return (
            <Droppable droppableId={AVAILABLE_ITEMS_KEY}>
                {(provided, snapshot) => (
                    <ul className="item-picker" ref={provided.innerRef} style={{ backgroundColor: snapshot.isDraggingOver ? 'yellow' : 'transparent' }}>
                        {availableItems.map(({ id, name, dietaries }, index) => (
                            <DraggableMenuItem
                                // I prefer assigning individual props, rather than spreading them,
                                // Avoids side-effects
                                key={`available-${id}`}
                                index={index}
                                id={id}
                                name={name}
                                dietaries={dietaries}
                                onClick={this.onAddItem(id)}
                            />
                        ))}
                        {provided.placeholder}
                    </ul>
                )}
            </Droppable>
        );
    }

    renderSelectedMenuItems() {
        const { selectedItems } = this;
        // Again, I would resolve this level of nesting with more time.
        // I feel the MenuItem example is enough as an example of what I would do
        return (
            <Droppable droppableId={SELECTED_ITEMS_KEY}>
                {(provided, snapshot) => (
                    <ul className="menu-preview" ref={provided.innerRef} style={{ backgroundColor: snapshot.isDraggingOver ? 'green' : 'transparent' }}>
                        {selectedItems.length ? selectedItems.map(({ id, name, dietaries }, index) => (
                            <DraggableMenuItem
                                key={`selected-${id}`}
                                index={index}
                                id={id}
                                name={name}
                                dietaries={dietaries}
                            >
                                <button className="remove-item" onClick={this.onRemoveItem(id)}>x</button>
                            </DraggableMenuItem>
                        )) : (
                            <li>
                                <aside><span role="img" aria-label="point left" >👈</span> Please add some items to your menu</aside>
                            </li>
                        )}
                        {provided.placeholder}
                    </ul>
                )}
            </Droppable>
        )
    }

    renderSelectedDietaryTotals() {
        const { selectedDietaryTotals } = this;
        return Object.keys(selectedDietaryTotals).map(dietary => (
            <React.Fragment key={`totals--${dietary}`}>
                <span>{selectedDietaryTotals[dietary]}x </span>
                <DietaryAttribute>{dietary}</DietaryAttribute>
            </React.Fragment>
        ));
    }

    render() {
        const { selectedTotal } = this;
        return (
            <div className="wrapper">
                <DragDropContext onDragEnd={this.onDragEnd}>
                    <div className="menu-summary">
                        <div className="container">
                            <div className="row">
                                <div className="col-6 menu-summary-left">
                                    <span>{selectedTotal} item{(selectedTotal !== 1) && 's'}</span>
                                </div>
                                <div className="col-6 menu-summary-right">
                                    {this.renderSelectedDietaryTotals()}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container menu-builder">
                        <div className="row">
                            <div className="col-4">
                                {this.renderAvailableMenuItems()}
                            </div>
                            <div className="col-8">
                                <h2>Menu preview</h2>
                                {this.renderSelectedMenuItems()}
                            </div>
                        </div>
                    </div>
                </DragDropContext>
            </div>
        );
    }
}

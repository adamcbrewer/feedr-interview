import React from 'react';
import { Draggable } from 'react-beautiful-dnd';

import DietaryAttribute from './DietaryAttribute';

// I would prefer to use a functional component, but react-beautiful-dnd was
// compaining about passing refs to them
export class MenuItem extends React.PureComponent {
    render() {
        const { id, name, dietaries = [], children, innerRef, ...props } = this.props;
        return (
            <li className="item" ref={innerRef} {...props}>
                <h2>{name}</h2>
                <p>
                    {dietaries.map(dietary =>
                        <DietaryAttribute key={`${id}-${dietary}`}>{dietary}</DietaryAttribute>)}
                </p>
                {children}
            </li>
        );
    }
}

export const DraggableMenuItem = ({ index, ...menuProps}) => (
    <Draggable index={index} draggableId={menuProps.id}>
        {(provided, snapshot) => (
            <MenuItem
                {...menuProps}
                innerRef={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
            />
        )}
    </Draggable>
)

export default MenuItem;
